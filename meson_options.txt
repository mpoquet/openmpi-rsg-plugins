option('ompi_src_path', type : 'string',
    value : '<ompi_src_path-UNSET>',
    description : 'OpenMPI source path (root)')

option('ompi_install_path', type : 'string',
    value : '<ompi_install_path-UNSET>',
    description : 'OpenMPI installation path')
