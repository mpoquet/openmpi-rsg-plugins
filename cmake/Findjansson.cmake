# Try to find jansson and validate that it is installed as it should be
# Once done it will define
# - JANSSON_FOUND
# - JANSSON_INCLUDE_DIRS
# - JANSSON_LIBRARIES

# We won't be using PkgConfig and maybe this should change in the future,
# all we are about to do is check if we can find the file <jansson.h> and
# the libjansson library (static or shared we don't care)

find_path(JANSSON_INCLUDE_DIRS jansson.h)
find_library(JANSSON_LIBRARIES jansson)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    jansson
    DEFAULT_MSG
    JANSSON_INCLUDE_DIRS
    JANSSON_LIBRARIES
)
