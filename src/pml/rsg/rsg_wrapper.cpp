#include "rsg_wrapper.hpp"

#include <stdio.h>

#include <algorithm>
#include <vector>
#include <unordered_map>

#include <rsg/actor.hpp>
#include <rsg/mailbox.hpp>
#include <rsg/comm.hpp>
#include <rsg/host.hpp>
#include <rsg/engine.hpp>

#include "logging.h"

#define RUN_WITH_GDB 0

static std::unordered_map<std::string, bool> _running_async_actors;
static std::vector<simgrid::rsg::Comm*> pending_comms;

struct MPIMessageData
{
    char * mailbox_name; // isend: the mailbox to send data to. irecv: the mailbox to receive data from.
    int is_blocking; // if true (1), the command has been issued from a blocking MPI call (MPI_Send, MPI_Recv...).
    char * ack_mailbox_name; // only for blocking calls. Used to acknowledge the blocking actor that the operation has been done. NULL for nonblocking calls.
    ompi_request_t * request; // the OpenMPI request associated with the call. NULL for blocking calls.
    void * buffer; // receive/send buffer depending on message type.
    size_t buffer_size; // buffer's size. For receive, this is USER buffer size. For send, this is USER buffer size + metadata.
    int expected_source; // only used for receive. can be MPI_ANY_SOURCE.
    int expected_tag; // only used for receive. can be MPI_ANY_TAG.
    int received_comm; // only used for receive. read from the message metadata once it has been received.
    int received_source; // only used for receive. read from the message metadata once it has been received.
    int received_tag; // only used for receive. read from the message metadata once it has been received.
    simgrid::rsg::Comm* comm; // the RSG communication used to do the data transfer associated with this message.
    bool is_transfer_finished; // whether the communication associated with this message is finished.
    bool is_request_finished; // whether the request associated with this message is marked as finished.
};

struct Command
{
    CommandType type;
    MPIMessageData * data;
};

enum class RequestType
{
    SEND,
    RECV
};

struct OMPIRequestInfo
{
    RequestType request_type;
    MPIMessageData * msg; // Got from PML's main thread/actor
    void * rsg_reception_buffer; // where RSG writes the data
};

// Associate OpenMPI requests to their RSG communication.
static std::unordered_map<simgrid::rsg::Comm*, OMPIRequestInfo*> pending_requests;
static std::list<OMPIRequestInfo*> pending_recv_requests; // recv requests whose comm is running
static std::list<OMPIRequestInfo*> finished_recv_requests; // recv requests whose comm has finished, but whose request has not been marked as finished because a matching request does not exist yet

// Whether the OpenMPI runtime to manage non-blocking communications is running.
static bool is_ompi_runtime_callback_running = false;
static int nb_finished_nonblocking_comm_since_last_callback_check = 0;

// This function can be called by OpenMPI's runtime after it has been registered (opal_progress_register).
// Returns the number of requests that have been finished during its execution.
int mca_pml_rsg_callback_for_nonblocking_comms(void)
{
    static const double sleep_duration = 1;
    debug_printf("sleeping for %g seconds...", sleep_duration);
    simgrid::rsg::this_actor::sleep(sleep_duration);
    int nb_comm_finished = nb_finished_nonblocking_comm_since_last_callback_check;
    nb_finished_nonblocking_comm_since_last_callback_check = 0;
    return nb_comm_finished;
}

static bool received_message_matches(int received_source, int received_tag, int expected_source, int expected_tag)
{
    if (expected_source == MPI_ANY_SOURCE)
    {
        if (expected_tag == MPI_ANY_TAG)
            return true;
        else
            return expected_tag == received_tag;
    }
    else
    {
        if (expected_tag == MPI_ANY_TAG)
            return expected_source == received_source;
        else
            return (expected_source == received_source) && (expected_tag == received_tag);
    }
}

static bool remove_first_value_from_list(std::list<OMPIRequestInfo*> & list, OMPIRequestInfo * value)
{
    auto it = std::find(list.begin(), list.end(), value);
    if (it != list.end())
    {
        list.erase(it);
        return true;
    }

    return false;
}

static std::list<OMPIRequestInfo*>::iterator find_request_matching_received_info(std::list<OMPIRequestInfo*> & requests, OMPIRequestInfo * needle)
{
    for (auto it = requests.begin(); it != requests.end(); ++it)
    {
        if (received_message_matches(needle->msg->received_source, needle->msg->received_tag, (*it)->msg->expected_source, (*it)->msg->expected_tag))
            return it;
    }

    return requests.end();
}

static std::list<OMPIRequestInfo*>::iterator find_request_matching_expected_info(std::list<OMPIRequestInfo*> & requests, OMPIRequestInfo * needle)
{
    for (auto it = requests.begin(); it != requests.end(); ++it)
    {
        if (received_message_matches((*it)->msg->received_source, (*it)->msg->received_tag, needle->msg->expected_source, needle->msg->expected_tag))
            return it;
    }

    return requests.end();
}

static void acknowledge_request_finition_to_blocking_actor(OMPIRequestInfo * finished)
{
    debug_printf("pid=%d. telling blocking actor that request %p has completed on mailbox '%s'", getpid(), finished->msg->request, finished->msg->ack_mailbox_name);
    simgrid::rsg::MailboxPtr ack_mailbox = simgrid::rsg::Mailbox::byName(finished->msg->ack_mailbox_name);
    int useless = 1;
    simgrid::rsg::this_actor::send(*ack_mailbox, (const char *)&useless, sizeof(int), sizeof(int));
}

static void mark_request_as_completed(OMPIRequestInfo * to_mark, OMPIRequestInfo * actual_buffer)
{
    to_mark->msg->is_request_finished = true;

    if (to_mark->request_type == RequestType::RECV && to_mark->msg->buffer_size > 0)
    {
        debug_printf("pid=%d. copying received buffer (from request %p) to user's buffer of matching request %p", getpid(), actual_buffer, to_mark);
        memcpy(to_mark->msg->buffer, ((char *)actual_buffer->rsg_reception_buffer) + 3 * sizeof(int), to_mark->msg->buffer_size);
    }

    if (!to_mark->msg->is_blocking)
    {
        debug_printf("pid=%d. marking request %p as complete", (int)getpid(), to_mark);
        ompi_request_complete(to_mark->msg->request, true);

        debug_printf("pid=%d. increase number of finished non blocking communications", (int)getpid());
        nb_finished_nonblocking_comm_since_last_callback_check++;
    }
    else
    {
        acknowledge_request_finition_to_blocking_actor(to_mark);
    }
}

static int async_actor_function(void * data)
{
    const char * mailbox_name = (const char *) data;
    debug_printf("mbox='%s' before get mailbox", mailbox_name);

    // The mailbox associated with this actor.
    simgrid::rsg::MailboxPtr mbox = simgrid::rsg::Mailbox::byName(mailbox_name);
    debug_printf("mbox='%s' after get mailbox", mailbox_name);
    mbox->setReceiver(simgrid::rsg::Actor::byPid(simgrid::rsg::this_actor::getPid()));
    debug_printf("mbox='%s' after setReceiver", mailbox_name);

#if RUN_WITH_GDB == 1
    insert_gdb_manual_breakpoint();
#endif

    Command * command = nullptr;
    simgrid::rsg::Comm * command_comm = &simgrid::rsg::Comm::recv_async(*mbox, (void**)&command);
    debug_printf("mbox='%s', command_comm=%p", mailbox_name, command_comm);
    pending_comms.push_back(command_comm);

    // Loop until termination is requested.
    for (bool termination_requested = false; !termination_requested; )
    {
        auto finished_it = simgrid::rsg::Comm::wait_any(&pending_comms);

        if (*finished_it == command_comm)
        {
            pending_comms.erase(finished_it);

            // Command received from the main actor.
            if (command->type == TERMINATE)
            {
                debug_printf("mbox='%s' TERMINATE", mailbox_name);
                termination_requested = true;
            }
            else if (command->type == NEW_SEND)
            {
                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_SEND (data=%p)", mailbox_name, (int)getpid(), command->data);
                MPIMessageData * msg = (MPIMessageData *) command->data;
                auto info = new OMPIRequestInfo{RequestType::SEND, msg, nullptr};

                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_SEND message received.\n\tompi_request=%p\n\tbuffer=%p\n\tbuffer_size=%zu)",
                             mailbox_name, (int)getpid(), msg->request, msg->buffer, msg->buffer_size);

                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_SEND getting destination mailbox '%s'", mailbox_name, (int)getpid(), msg->mailbox_name);
                simgrid::rsg::MailboxPtr destination_mailbox = simgrid::rsg::Mailbox::byName(msg->mailbox_name);

                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_SEND initiate send on '%s'", mailbox_name, (int)getpid(), msg->mailbox_name);
                info->msg->comm = &simgrid::rsg::Comm::send_async(*destination_mailbox, msg->buffer, msg->buffer_size, (int)msg->buffer_size);

                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_SEND marking comm %p (and its request %p) as pending", mailbox_name, (int)getpid(), info->msg->comm, msg->request);
                pending_comms.push_back(info->msg->comm);
                pending_requests[info->msg->comm] = info;
            }
            else if (command->type == NEW_RECV)
            {
                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_RECV (data=%p)", mailbox_name, (int)getpid(), command->data);
                MPIMessageData * msg = (MPIMessageData *) command->data;

                debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_RECV message received.\n\tompi_request=%p\n\tbuffer=%p\n\tbuffer_size=%zu)",
                             mailbox_name, (int)getpid(), msg->request, msg->buffer, msg->buffer_size);
                auto info = new OMPIRequestInfo{RequestType::RECV, msg, nullptr};

                // First, check whether a matching message has already been received.
                if (auto found_it = find_request_matching_expected_info(finished_recv_requests, info); found_it != finished_recv_requests.end())
                {
                    // No need for a new RSG comm, a matching message has already been received.
                    debug_printf("pid=%d no need for a new RSG reception, a matching message has already been received (info=%p)", getpid(), *found_it);
                    mark_request_as_completed(info, *found_it);
                    finished_recv_requests.erase(found_it);

                    delete info;
                }
                else
                {
                    // Start a new RSG reception to manage the request.
                    debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_RECV getting reception mailbox '%s'", mailbox_name, (int)getpid(), msg->mailbox_name);
                    simgrid::rsg::MailboxPtr reception_mailbox = simgrid::rsg::Mailbox::byName(msg->mailbox_name);

                    debug_printf("mbox='%s' pid=%d NEW_NONBLOCKING_RECV initiate recv on '%s'", mailbox_name, (int)getpid(), msg->mailbox_name);
                    info->msg->comm = &simgrid::rsg::Comm::recv_async(*reception_mailbox, (void**)&info->rsg_reception_buffer);
                    pending_comms.push_back(info->msg->comm);
                    pending_requests[info->msg->comm] = info;
                    pending_recv_requests.push_back(info);
                }
            }

            debug_printf("mbox='%s' pid=%d cleaning memory of received command", mailbox_name, (int)getpid());
            free(command);
            command = nullptr;
            command_comm = nullptr;

            if (!termination_requested)
            {
                debug_printf("mbox='%s' pid=%d creating another comm to receive commands", mailbox_name, (int)getpid());
                command_comm = &simgrid::rsg::Comm::recv_async(*mbox, (void**)&command);
                debug_printf("mbox='%s', command_comm=%p", mailbox_name, command_comm);
                pending_comms.push_back(command_comm);
            }
        }
        else
        {
            // A message comm finished! It corresponds to a buffer transfer from one rank to another (potentially the same)
            simgrid::rsg::Comm * comm = *finished_it;
            debug_printf("pid=%d. non-blocking comm %p has finished", getpid(), comm);

            OMPIRequestInfo *info = pending_requests.at(comm);
            ompi_request_t * request = info->msg->request;
            debug_printf("pid=%d. non-blocking comm %p has info %p and request %p", getpid(), comm, info, request);

            info->msg->is_transfer_finished = true;

            if (info->request_type == RequestType::RECV)
            {
                debug_printf("pid=%d. request %p is a RECV one", (int)getpid(), request);
                // Read message metadata.
                memcpy(&info->msg->received_comm,   ((char *)info->rsg_reception_buffer) + 0 * sizeof(int), sizeof(int));
                memcpy(&info->msg->received_source, ((char *)info->rsg_reception_buffer) + 1 * sizeof(int), sizeof(int));
                memcpy(&info->msg->received_tag,    ((char *)info->rsg_reception_buffer) + 2 * sizeof(int), sizeof(int));
                debug_printf("pid=%d. %p is a RECV request. (comm=%d, source=%d, tag=%d)", (int)getpid(), request, info->msg->received_comm, info->msg->received_source, info->msg->received_tag);

                // Does the received message matches the one requested by the user in this the call that issued this communication?
                // This is not always the case because of MPI_ANY_SOURCE and MPI_ANY_TAG.
                if (!info->msg->is_request_finished && received_message_matches(info->msg->received_source, info->msg->received_tag, info->msg->expected_source, info->msg->expected_tag))
                {
                    // Good, that's the straightforward way. Received data is compatible with the user request whose comm has just finished.
                    debug_printf("pid=%d. Received message matches the request %p that issued it", (int)getpid(), info->msg->request);
                    mark_request_as_completed(info, info);
                    remove_first_value_from_list(pending_recv_requests, info);
                }
                else
                {
                    // So, this is not the straightforward way...
                    // Store the message in a dedicated set, so a future communication can match it.
                    debug_printf("pid=%d. receive request %p does not match any known request. storing the request in a dedicated set so a future user recv can match %p.", (int)getpid(), request, info);
                    finished_recv_requests.push_back(info);

                    debug_printf("pid=%d. remove info %p from pending_recv_requests, as its communication has completed.", getpid(), info);
                    remove_first_value_from_list(pending_recv_requests, info);

                    // Check whether a request whose communication has finished but that is not marked as finished yet matches the received message.
                    debug_printf("pid=%d. traversing the set of finished recv requests, to check if the recently finished comm matches one of them", (int)getpid());
                    if (auto found_it = find_request_matching_received_info(finished_recv_requests, info); found_it != finished_recv_requests.end())
                    {
                        // There is a match! A request that ended previously can finally be marked as done.
                        debug_printf("pid=%d. finished comm matches the already finished request %p!", (int)getpid(), *found_it);
                        mark_request_as_completed(*found_it, info);
                        finished_recv_requests.erase(found_it);
                    }

                    // Does the received message matches any known request whose communication has not been done yet?
                    if (auto found_it = find_request_matching_received_info(pending_recv_requests, info); found_it != pending_recv_requests.end())
                    {
                        // A communication that is going on matches the received message.
                        debug_printf("pid=%d. Received message matches a known communication that is going on. This is on info %p, which is not the straightforward one", (int)getpid(), *found_it);
                        mark_request_as_completed(*found_it, info);
                        pending_recv_requests.erase(found_it);
                    }

                    if (!info->msg->is_request_finished)
                    {
                        // The request that issued the message that just terminated is not marked as finished yet.
                        // This can cause a deadlock. For example when a rank only has one pending receive comm that did not receive the expected message.
                        // To avoid it, a new RSG communication is launched — with the exact same properties as those of the initial user request.
                        // Note that to keep a valid number of comms, the program must check existing buffered received messages before creating a new comm.
                        debug_printf("pid=%d. As the request of the message that just have been received is still pending, issuing a new RSG recv similar to the requested one.", (int)getpid());

                        // Instantiate a new MPIMessageData
                        auto new_data = (MPIMessageData *)malloc(sizeof(MPIMessageData));
                        new_data->mailbox_name = strdup(info->msg->mailbox_name);
                        new_data->is_blocking = info->msg->is_blocking;
                        new_data->ack_mailbox_name = (info->msg->ack_mailbox_name == nullptr) ? nullptr : strdup(info->msg->ack_mailbox_name);
                        new_data->request = info->msg->request;
                        new_data->buffer = info->msg->buffer;
                        new_data->buffer_size = info->msg->buffer_size;
                        new_data->expected_source = info->msg->expected_source;
                        new_data->expected_tag = info->msg->expected_tag;
                        new_data->received_comm = -666;
                        new_data->received_source = -666;
                        new_data->received_tag = -666;
                        new_data->is_transfer_finished = false;
                        new_data->is_request_finished = false;

                        // Instantiate new info
                        auto new_info = new OMPIRequestInfo{RequestType::RECV, new_data, nullptr};

                        // Start new RSG comm
                        auto reception_mailbox = simgrid::rsg::Mailbox::byName(new_data->mailbox_name);
                        new_info->msg->comm = &simgrid::rsg::Comm::recv_async(*reception_mailbox, (void**)&new_info->rsg_reception_buffer);
                        pending_comms.push_back(new_info->msg->comm);
                        pending_requests[new_info->msg->comm] = new_info;
                        pending_recv_requests.push_back(new_info);
                    }
                }

            }
            else // Request type is a SEND
            {
                debug_printf("pid=%d. request %p is a SEND one", (int)getpid(), request);
                mark_request_as_completed(info, info);
            }

            debug_printf("pid=%d. removing finished comm %p from pending_comms", getpid(), comm);
            pending_comms.erase(finished_it);
        }
    }

    debug_printf("Quitting");
    simgrid::rsg::this_actor::quit();
    return 0;
}

void run_ompi_runtime_callback_if_needed()
{
    if (!is_ompi_runtime_callback_running)
    {
        is_ompi_runtime_callback_running = true;
        debug_printf("pid=%d registering OPAL progress callback to manage request finition", getpid());
        opal_progress_register(mca_pml_rsg_callback_for_nonblocking_comms);
    }
}

void stop_ompi_runtime_callback_if_needed()
{
    if (is_ompi_runtime_callback_running)
    {
        debug_printf("pid=%d. unregistering OPAL progress callback function", getpid());
        is_ompi_runtime_callback_running = false;
        opal_progress_unregister(mca_pml_rsg_callback_for_nonblocking_comms);
    }
}


void spawn_async_actor_if_needed(char *unique_name)
{
    std::string name(unique_name);
    if (_running_async_actors.count(name) == 0)
    {
        _running_async_actors.insert({name, true});
        simgrid::rsg::Actor::createActor(unique_name, simgrid::rsg::Host::current(), async_actor_function, unique_name);
    }
}

void terminate_async_actors()
{
    debug_printf("Terminating async actors...");
    //insert_gdb_manual_breakpoint();
    for (const auto & it : _running_async_actors)
    {
        const std::string & name = it.first;
        Command * command = (Command *) malloc(sizeof(Command));
        command->type = TERMINATE;
        command->data = nullptr;

        debug_printf("Terminating async actor '%s'...", name.c_str());
        simgrid::rsg::MailboxPtr mailbox = simgrid::rsg::Mailbox::byName(name.c_str());
        simgrid::rsg::this_actor::send(*mailbox, (const char *)command, sizeof(Command));
    }
}

void send_command_to_async_actor(char * unique_name, void *command)
{
    Command * cmd = (Command *) command;
    debug_printf("mbox='%s' before get mailbox", unique_name);
    simgrid::rsg::MailboxPtr mailbox = simgrid::rsg::Mailbox::byName(unique_name);
    /*debug_printf("mbox='%s' before send", unique_name);
    simgrid::rsg::this_actor::send(*mailbox, (const char *)cmd, sizeof(Command));
    debug_printf("mbox='%s' after send", unique_name);*/
    debug_printf("mbox='%s' before send_async. system_pid=%d\n\tcommand=%p\n\tcommand_data=%p\n\tcommand_msg_buffer=%p\n\tcommand_msg_buffer_length=%zu\n\tRSG_msg_size=%zu",
                 unique_name, (int)getpid(), cmd, cmd->data, ((MPIMessageData*)cmd->data)->buffer, ((MPIMessageData*)cmd->data)->buffer_size, sizeof(Command));
    //insert_gdb_manual_breakpoint();
    simgrid::rsg::Comm::send_async(*mailbox, command, sizeof(Command));
    debug_printf("mbox='%s' after send_async", unique_name);
}

// quit
void rsg_quit()
{
    debug_printf("quitting this actor");
    simgrid::rsg::this_actor::quit();
    debug_printf("quitting this actor done");
}

// sleep
void rsg_sleep(double duration)
{
    debug_printf("before sleep");
    simgrid::rsg::this_actor::sleep(duration);
    debug_printf("after sleep");
}

// compute
void rsg_compute(double nb_flops)
{
    debug_printf("before compute");
    simgrid::rsg::this_actor::execute(nb_flops);
    debug_printf("after compute");
}

// send
void rsg_send_msg(const char *mailbox_name, const char *buf, size_t buf_len)
{
    debug_printf("get mailbox '%s'", mailbox_name);
    simgrid::rsg::MailboxPtr mailbox = simgrid::rsg::Mailbox::byName(mailbox_name);

    debug_printf("send buffer (data=%p, length=%zu) on '%s'", buf, buf_len, mailbox_name);
    simgrid::rsg::this_actor::send(*mailbox, (const char *)buf, buf_len);

    debug_printf("sending buffer (data=%p, length=%zu) on '%s' finished. Leaving function.", buf, buf_len, mailbox_name);
}

struct SendData
{
    char * mailbox_name;
    char * buf;
    size_t buf_len;
};

class SendFunctor
{
public:
    SendFunctor(SendData * data) : sdata(data) {}
    SendData * sdata;
    int operator()(void *)
    {
        debug_printf("calling rsg_send_msg");
        rsg_send_msg(sdata->mailbox_name, sdata->buf, sdata->buf_len);
        return 0;
    }
};

static void* rsg_send_msg_thread_function(void * data)
{
    // Get data
    debug_printf("reading data");
    SendData * sdata = (SendData*) data;

    // Do the actual sending in another actor
    debug_printf("creating actor");
    simgrid::rsg::Actor* actor = simgrid::rsg::Actor::createActor("sender", simgrid::rsg::Host::current(), SendFunctor(sdata), NULL);

    // Wait for the actor finition (and therefore the send finition)
    debug_printf("waiting for actor finition");
    actor->join();

    // Memory cleanup
    free(sdata->mailbox_name);
    free(sdata->buf);
    delete sdata;

    return nullptr;
}

void rsg_send_msg_nonblocking(pthread_t * thread, char *mailbox_name, char *buf, size_t buf_len)
{
    SendData * sdata = new SendData;
    sdata->mailbox_name = mailbox_name;
    sdata->buf = buf;
    sdata->buf_len = buf_len;

    debug_printf("Creating thread");
    if (pthread_create(thread, NULL, rsg_send_msg_thread_function, (void*)sdata) != 0)
    {
        debug_printf("Could not create thread :(");
    }
}

// recv
void rsg_recv_msg(const char *mailbox_name, char ** buf)
{
    debug_printf("get mailbox '%s'", mailbox_name);
    simgrid::rsg::MailboxPtr mailbox = simgrid::rsg::Mailbox::byName(mailbox_name);

    debug_printf("receive buffer from '%s'", mailbox_name);
    *buf = simgrid::rsg::this_actor::recv(*mailbox);

    debug_printf("received buffer from '%s', leaving function", mailbox_name);
}

struct RecvData
{
    char * mailbox_name;
    char * buf;
};

class RecvFunctor
{
public:
    RecvFunctor(RecvData * data) : rdata(data) {}
    RecvData * rdata;
    int operator()(void *)
    {
        debug_printf("calling rsg_recv_msg");
        rsg_recv_msg(rdata->mailbox_name, &rdata->buf);
        return 0;
    }
};

static void* rsg_recv_msg_thread_function(void * data)
{
    // Get data
    debug_printf("reading data");
    RecvData * rdata = (RecvData*) data;

    // Do the actual receive in another actor
    debug_printf("creating actor");
    simgrid::rsg::Actor* actor = simgrid::rsg::Actor::createActor("receiver", simgrid::rsg::Host::current(), RecvFunctor(rdata), NULL);

    // Wait for the actor finition (and therefore the receive finition)
    debug_printf("waiting for actor finition");
    actor->join();

    // Memory cleanup
    free(rdata->mailbox_name);
    free(rdata->buf);
    delete rdata;

    return nullptr;
}

void rsg_recv_msg_nonblocking(pthread_t * thread, char *mailbox_name)
{
    RecvData * rdata = new RecvData;
    rdata->mailbox_name = mailbox_name;
    rdata->buf = nullptr;

    debug_printf("Creating thread");
    if (pthread_create(thread, NULL, rsg_recv_msg_thread_function, (void*)rdata) != 0)
    {
        debug_printf("Could not create thread :(");
    }
}

// handy tests
void rsg_test_sendrecv(unsigned int rank)
{
    if (rank == 0)
    {
        int data = 42;
        debug_printf("rank=%d. sending data=%d...", rank, data);
        rsg_send_msg("mailbox", (char *)&data, sizeof(int));
        debug_printf("rank=%d. sent data=%d", rank, data);
    }
    else if (rank == 1)
    {
        int * data;
        debug_printf("rank=%d. receiving data...", rank);
        rsg_recv_msg("mailbox", (char**)&data);
        debug_printf("rank=%d. received data=%d", rank, *data);
        free(data);
    }
}

static void* rsg_test_sendrecv_thread_function(void * data)
{
    unsigned int rank = *((unsigned int*)data);
    rsg_test_sendrecv(rank);
    return nullptr;
}

void rsg_test_sendrecv_nonblocking(pthread_t * thread, unsigned int rank)
{
    pthread_create(thread, NULL, rsg_test_sendrecv_thread_function, (void*)&rank);
}

void rsg_test_compute(unsigned int rank)
{
    double flop_amount = 1000;
    debug_printf("rank=%d. computing flops=%g...", rank, flop_amount);
    simgrid::rsg::this_actor::execute(flop_amount);
    debug_printf("rank=%d. computed flops=%g", rank, flop_amount);
}

static unsigned int rsg_test_global = 0;

void rsg_test_setglobal(unsigned int value)
{
    rsg_test_global = value;
}

unsigned int rsg_test_getglobal()
{
    return rsg_test_global;
}

void insert_gdb_manual_breakpoint()
{
    printf("pid %d for attach\n", getpid());
    fflush(stdout);

    volatile int attached = 0;
    while (!attached)
    {
        sleep(1);
    }
}

void forward_message_to_nonblocking_actor(char * nonblocking_mailbox_name, // the mailbox on which the nonblocking actor waits the command
                                          char * communication_mailbox_name, // the mailbox used to send/receive the buffer
                                          char * ack_mailbox_name, // the mailbox used to acknowledge the blocking actor that the operation has been done
                                          CommandType command_type,
                                          int is_blocking,
                                          void * buffer,
                                          size_t buffer_size,
                                          int expected_source,
                                          int expected_tag,
                                          ompi_request_t * request)
{
    // Spawn the nonblocking actor if it not running.
    spawn_async_actor_if_needed(nonblocking_mailbox_name);

    // Register OpenMPI's runtime callback if it is not already done.
    run_ompi_runtime_callback_if_needed();

    // Instantiate data structures to forward data to the nonblocking actor.
    MPIMessageData * msg = (MPIMessageData *) malloc(sizeof(MPIMessageData));
    msg->mailbox_name = communication_mailbox_name;
    msg->is_blocking = is_blocking;
    msg->ack_mailbox_name = ack_mailbox_name;
    msg->request = request;
    msg->buffer = buffer;
    msg->buffer_size = buffer_size;
    msg->expected_source = expected_source;
    msg->expected_tag = expected_tag;
    msg->received_comm = -666;
    msg->received_source = -666;
    msg->received_tag = -666;
    msg->comm = nullptr;
    msg->is_transfer_finished = false;
    msg->is_request_finished = false;

    Command * cmd = (Command *) malloc(sizeof(Command));
    cmd->type = command_type;
    cmd->data = msg;

    // Finally forward the isend to the nonblocking actor.
    send_command_to_async_actor(nonblocking_mailbox_name, cmd);
}
