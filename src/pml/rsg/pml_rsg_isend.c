/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2004-2005 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2004-2015 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2004-2005 High Performance Computing Center Stuttgart,
 *                         University of Stuttgart.  All rights reserved.
 * Copyright (c) 2004-2005 The Regents of the University of California.
 *                         All rights reserved.
 * Copyright (c) 2007-2016 Los Alamos National Security, LLC.  All rights
 *                         reserved.
 * Copyright (c) 2014      Cisco Systems, Inc.  All rights reserved.
 * Copyright (c) 2015      Research Organization for Information Science
 *                         and Technology (RIST). All rights reserved.
 * $COPYRIGHT$
 *
 * Additional copyrights may follow
 *
 * $HEADER$
 */

#include "ompi_config.h"

#include "pml_rsg.h"
#include "pml_rsg_sendreq.h"
#include "pml_rsg_recvreq.h"
#include "logging.h"
#include "rsg_wrapper.hpp"
#include "ompi/peruse/peruse-internal.h"

/**
 * Single usage request. As we allow recursive calls (as an
 * example from the request completion callback), we cannot rely
 * on using a global request. Thus, once a send acquires ownership
 * of this global request, it should set it to NULL to prevent
 * the reuse until the first user completes.
 */
mca_pml_rsg_send_request_t *mca_pml_rsg_sendreq = NULL;

int mca_pml_rsg_isend_init(const void *buf,
                           size_t count,
                           ompi_datatype_t * datatype,
                           int dst,
                           int tag,
                           mca_pml_base_send_mode_t sendmode,
                           ompi_communicator_t * comm,
                           ompi_request_t ** request)
{
    debug_printf("my_rank=%d dst=%d tag=%d", OPAL_PROC_MY_NAME.vpid, dst, tag);
    mca_pml_rsg_send_request_t *sendreq = NULL;
    MCA_PML_OB1_SEND_REQUEST_ALLOC(comm, dst, sendreq);
    if (NULL == sendreq)
        return OMPI_ERR_OUT_OF_RESOURCE;

    MCA_PML_OB1_SEND_REQUEST_INIT(sendreq,
                                  buf,
                                  count,
                                  datatype,
                                  dst, tag,
                                  comm, sendmode, true);

    PERUSE_TRACE_COMM_EVENT (PERUSE_COMM_REQ_ACTIVATE,
                             &(sendreq)->req_send.req_base,
                             PERUSE_SEND);

    /* Work around a leak in start by marking this request as complete. The
     * problem occured because we do not have a way to differentiate an
     * inital request and an incomplete pml request in start. This line
     * allows us to detect this state. */
    sendreq->req_send.req_base.req_pml_complete = true;

    *request = (ompi_request_t *) sendreq;
    return OMPI_SUCCESS;
}

/* try to get a small message out on to the wire quickly */
static inline int mca_pml_rsg_send_inline (const void *buf, size_t count,
                                           ompi_datatype_t * datatype,
                                           int dst, int tag, int16_t seqn,
                                           ompi_proc_t *dst_proc, mca_bml_base_endpoint_t* endpoint,
                                           ompi_communicator_t * comm)
{
    mca_pml_rsg_match_hdr_t match;
    mca_bml_base_btl_t *bml_btl;
    opal_convertor_t convertor;
    size_t size;
    int rc;

    bml_btl = mca_bml_base_btl_array_get_next(&endpoint->btl_eager);
    if( NULL == bml_btl->btl->btl_sendi)
        return OMPI_ERR_NOT_AVAILABLE;

    ompi_datatype_type_size (datatype, &size);
    if ((size * count) > 256) {  /* some random number */
        return OMPI_ERR_NOT_AVAILABLE;
    }

    if (count > 0) {
        /* initialize just enough of the convertor to avoid a SEGV in opal_convertor_cleanup */
        OBJ_CONSTRUCT(&convertor, opal_convertor_t);

        /* We will create a convertor specialized for the        */
        /* remote architecture and prepared with the datatype.   */
        opal_convertor_copy_and_prepare_for_send (dst_proc->super.proc_convertor,
                                                  (const struct opal_datatype_t *) datatype,
                                                  count, buf, 0, &convertor);
        opal_convertor_get_packed_size (&convertor, &size);
    } else {
        size = 0;
    }

    mca_pml_rsg_match_hdr_prepare (&match, MCA_PML_OB1_HDR_TYPE_MATCH, 0,
                                   comm->c_contextid, comm->c_my_rank,
                                   tag, seqn);

    ob1_hdr_hton(&match, MCA_PML_OB1_HDR_TYPE_MATCH, dst_proc);

    /* try to send immediately */
    rc = mca_bml_base_sendi (bml_btl, &convertor, &match, OMPI_PML_OB1_MATCH_HDR_LEN,
                             size, MCA_BTL_NO_ORDER, MCA_BTL_DES_FLAGS_PRIORITY | MCA_BTL_DES_FLAGS_BTL_OWNERSHIP,
                             MCA_PML_OB1_HDR_TYPE_MATCH, NULL);
    if (count > 0) {
        opal_convertor_cleanup (&convertor);
    }

    if (OPAL_UNLIKELY(OMPI_SUCCESS != rc)) {
	return rc;
    }

    return (int) size;
}

int mca_pml_rsg_isend(const void *buf,
                      size_t count,
                      ompi_datatype_t * datatype,
                      int dst,
                      int tag,
                      mca_pml_base_send_mode_t sendmode,
                      ompi_communicator_t * comm,
                      ompi_request_t ** request)
{
    const struct opal_datatype_t * opal_datatype = (const struct opal_datatype_t *) datatype;
    debug_printf("my_rank=%d my_pid=%d dst=%d tag=%d count=%zu datatype_size=%zu comm=(name=%s, contextid=%d)", OPAL_PROC_MY_NAME.vpid, getpid(), dst, tag, count, opal_datatype->size, comm->c_name, comm->c_contextid);

    // Copy buffer into a bigger one that contains (communicator, source, tag)
    char * buf_with_metadata = malloc(count * opal_datatype->size + 3 * sizeof(int));
    memcpy(buf_with_metadata + 0*sizeof(int), &comm->c_contextid, sizeof(int));
    memcpy(buf_with_metadata + 1*sizeof(int), &comm->c_my_rank, sizeof(int));
    memcpy(buf_with_metadata + 2*sizeof(int), &tag, sizeof(int));
    memcpy(buf_with_metadata + 3*sizeof(int), buf, count * opal_datatype->size);

    // Prepare request
    mca_pml_rsg_comm_proc_t *ob1_proc = mca_pml_rsg_peer_lookup (comm, dst);
    mca_pml_rsg_send_request_t *sendreq = NULL;
    ompi_proc_t *dst_proc = ob1_proc->ompi_proc;
    mca_bml_base_endpoint_t* endpoint = mca_bml_base_get_endpoint (dst_proc);
    int16_t seqn = 0;

    if (OPAL_UNLIKELY(NULL == endpoint)) {
        return OMPI_ERR_UNREACH;
    }

    if (!OMPI_COMM_CHECK_ASSERT_ALLOW_OVERTAKE(comm)) {
        seqn = (uint16_t) OPAL_THREAD_ADD32(&ob1_proc->send_sequence, 1);
    }

    debug_printf("my_rank=%d, calling MCA_PML_OB1_SEND_REQUEST_ALLOC", OPAL_PROC_MY_NAME.vpid);
    MCA_PML_OB1_SEND_REQUEST_ALLOC(comm, dst, sendreq);
    if (NULL == sendreq)
        return OMPI_ERR_OUT_OF_RESOURCE;

    debug_printf("my_rank=%d, calling MCA_PML_OB1_SEND_REQUEST_INIT", OPAL_PROC_MY_NAME.vpid);
    MCA_PML_OB1_SEND_REQUEST_INIT(sendreq,
                                  buf,
                                  count,
                                  datatype,
                                  dst, tag,
                                  comm, sendmode, false);

    debug_printf("my_rank=%d, calling PERUSE_TRACE_COMM_EVENT", OPAL_PROC_MY_NAME.vpid);
    PERUSE_TRACE_COMM_EVENT (PERUSE_COMM_REQ_ACTIVATE,
                             &(sendreq)->req_send.req_base,
                             PERUSE_SEND);

    //////////////////////////////////////////////////////////////////////////////////////////
    // MCA_PML_OB1_SEND_REQUEST_START_W_SEQ(sendreq, endpoint, seqn, rc);
    sendreq->req_state = 0;
    sendreq->req_lock = 0;
    sendreq->req_pipeline_depth = 0;
    sendreq->req_bytes_delivered = 0;
    sendreq->req_pending = MCA_PML_OB1_SEND_PENDING_NONE;
    MCA_PML_BASE_SEND_START(&sendreq->req_send);
    //////////////////////////////////////////////////////////////////////////////////////////
    *request = (ompi_request_t *) sendreq;

    // Generate mailboxes' name
    char * nonblocking_mailbox_name;
    asprintf(&nonblocking_mailbox_name, "nb %d %d", comm->c_contextid, comm->c_my_rank);
    char * send_mailbox_name;
    asprintf(&send_mailbox_name, "%d %d", comm->c_contextid, dst);

    // Forward the isend to the nonblocking actor
    forward_message_to_nonblocking_actor(nonblocking_mailbox_name, send_mailbox_name, NULL, NEW_SEND, 0,
        buf_with_metadata, count * opal_datatype->size + 3 * sizeof(int), -42, tag,
        *request);

    return OMPI_SUCCESS;
}

int mca_pml_rsg_send(const void *buf,
                     size_t count,
                     ompi_datatype_t * datatype,
                     int dst,
                     int tag,
                     mca_pml_base_send_mode_t sendmode,
                     ompi_communicator_t * comm)
{
    const struct opal_datatype_t * opal_datatype = (const struct opal_datatype_t *) datatype;
    debug_printf("my_rank=%d my_pid=%d dst=%d tag=%d count=%zu datatype_size=%zu comm=(name=%s, contextid=%d)", OPAL_PROC_MY_NAME.vpid, getpid(), dst, tag, count, opal_datatype->size, comm->c_name, comm->c_contextid);

    // Copy buffer into a bigger one that contains (communicator, source, tag)
    char * buf_with_metadata = malloc(count * opal_datatype->size + 3 * sizeof(int));
    memcpy(buf_with_metadata + 0*sizeof(int), &comm->c_contextid, sizeof(int));
    memcpy(buf_with_metadata + 1*sizeof(int), &comm->c_my_rank, sizeof(int));
    memcpy(buf_with_metadata + 2*sizeof(int), &tag, sizeof(int));
    memcpy(buf_with_metadata + 3*sizeof(int), buf, count * opal_datatype->size);

    // Generate mailboxes' name
    char * nonblocking_mailbox_name;
    asprintf(&nonblocking_mailbox_name, "nb %d %d", comm->c_contextid, comm->c_my_rank);
    char * send_mailbox_name;
    asprintf(&send_mailbox_name, "%d %d", comm->c_contextid, dst);
    char * ack_mailbox_name;
    asprintf(&ack_mailbox_name, "b %d %d", comm->c_contextid, comm->c_my_rank);

    // Forward the send to the nonblocking actor
    forward_message_to_nonblocking_actor(nonblocking_mailbox_name, send_mailbox_name, ack_mailbox_name, NEW_SEND, 1,
        buf_with_metadata, count * opal_datatype->size + 3 * sizeof(int), -42, tag,
        NULL);

    // Wait for the communication completion. This means waiting for acknowledgement from the nonblocking actor.
    char * data;
    rsg_recv_msg(ack_mailbox_name, &data);

    return OMPI_SUCCESS;
}
