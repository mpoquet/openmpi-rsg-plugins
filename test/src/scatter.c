#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    printf("Rank=%d. ENV(RsgRpcNetworkName) = %s\n",
        rank, getenv("RsgRpcNetworkName"));

    const int recv_array_size = 100;
    const int send_array_size = 100 * size;

    int * send_array = NULL;
    int * recv_array = calloc(recv_array_size, sizeof(int));

    // Data: an array of 100*size integers. Values should be 100 0, 100 1, 100 2 on rank 0...
    if (rank == 0)
    {
        send_array = calloc(send_array_size, sizeof(int));

        // Initialize send_array
        for (int i = 0; i < send_array_size; i++)
        {
            send_array[i] = i / 100;
        }
    }

    // Precheck
    if (rank == 0)
    {
        assertf(send_array != NULL, "rank=%d. send_array is %p (expected different from %p)", rank, send_array, NULL);
        for (int i = 0; i < send_array_size; i++)
        {
            assertf(send_array[i] == i/100, "rank=%d. cell %d of send_array is %d (expected %d)", rank, i, send_array[i], i/100);
        }
    }
    else
    {
        assertf(send_array == NULL, "rank=%d. send_array is %p (expected %p)", rank, send_array, NULL);
    }

    for (int i = 0; i < recv_array_size; i++)
    {
        assertf(recv_array[i] == 0, "rank=%d. cell %d of recv_array is %d (expected %d)", rank, i, recv_array[i], 0);
    }

    // Scatter
    MPI_Scatter(send_array, recv_array_size, MPI_INT, recv_array, recv_array_size, MPI_INT, 0, MPI_COMM_WORLD);

    // Postcheck
    if (rank == 0)
    {
        assertf(send_array != NULL, "rank=%d. send_array is %p (expected different from %p)", rank, send_array, NULL);
        for (int i = 0; i < send_array_size; i++)
        {
            assertf(send_array[i] == i/100, "rank=%d. cell %d of send_array is %d (expected %d)", rank, i, send_array[i], i/100);
        }
    }
    for (int i = 0; i < recv_array_size; i++)
    {
        assertf(recv_array[i] == rank, "rank=%d. cell %d of recv_array is %d (expected %d)", rank, i, recv_array[i], rank);
    }

    free(send_array);
    free(recv_array);

    MPI_Finalize();
    return 0;
}
