#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_error(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define assertf(A, M, ...) if(!(A)) {log_error(M, ##__VA_ARGS__); assert(A); }

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Data: an array of 1000 integers. All values should equal rank.
    const int array_size = 1000;
    int * send_array = calloc(array_size, sizeof(int));
    int * recv_array = calloc(array_size, sizeof(int));

    // Initialize send_array
    for (int i = 0; i < array_size; i++)
    {
        send_array[i] = rank;
    }

    MPI_Request requests[1];
    MPI_Status statuses[1];
    MPI_Isend(send_array, array_size, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[0]);
    MPI_Waitall(1, requests, statuses);
    // Code should NOT reach this line.
    MPI_Irecv(recv_array, array_size, MPI_INT, rank, 0, MPI_COMM_WORLD, &requests[0]);
    MPI_Waitall(1, requests, statuses);

    free(send_array);
    free(recv_array);

    MPI_Finalize();
    return 0;
}
