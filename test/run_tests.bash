#!/usr/bin/env bash

function usage {
    echo "Usage: $0 [options] TEST..."
    echo "Run some openmpi-rsg-plugins tests with the given test execution wrapper."
    echo ""
    echo "Option list."
    echo "  -w, --wrap=PREFIX     command prefix for each test"
    echo "  -b, --build-dir=DIR   build directory. [default: ./build]"
    echo ""
    echo "Example usage."
    echo "  $0"
    echo "  $0 -w \"mpirun --mca async_mpi_finalize 1 --mca odls default --mca pml ob1 --mca btl self,tcp -np 2\""
    echo "  $0 -w \"mpirun --mca async_mpi_finalize 1 --mca odls rsg --mca pml rsg --mca btl self,tcp\""
    echo "  $0 hello gather"
}

function cleanup {
    killall rsg_server 2>/dev/null
    rm -f ./.meson_test.out
}

# Read input arguments.
test_wrap=""
build_dir="./build"
test_names="<unset>"
while [ "$1" != "" ] ; do
    case $1 in
        -w | --wrap )           shift
                                test_wrap=$1
                                ;;
        -b | --build-dir )      shift
                                build_dir=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
                                # Put remaining arguments into the test_names array
        * )                     readarray -t -d ' ' test_names <<<"$@"; declare -p test_names;
                                shift $#
    esac
    shift
done

# Beginning of the script.
trap cleanup EXIT # Just in case.

# If unset, retrieve test names from the list of all meson tests.
if [ "${test_names}" == "<unset>" ] ; then
    test_list_string=$(meson test -C ${build_dir} --list)
    readarray -t test_names <<<"${test_list_string}"; declare -p test_names;
fi

# Run tests.
successful_tests=()
failed_tests=()
for test_name in "${test_names[@]}" ; do
    cleanup
    echo "Launching test ${test_name}"
    ./run.bash -t 10 -d 3 ${test_wrap} ${build_dir}/${test_name}
    return_code=$?
    if [ ${return_code} -ne 0 ] ; then
        failed_tests+=(${test_name})
    else
        successful_tests+=(${test_name})
    fi
done
cleanup

# Print result summary.
if [ ${#failed_tests[@]} -eq 0 ] && [ ${#successful_tests[@]} -eq 0 ] ; then
    echo "No test has been run..."
elif [ ${#failed_tests[@]} -eq 0 ] ; then
    echo "All tests were successful."
fi

if [ ${#failed_tests[@]} -gt 0 ] ; then
    echo "The following tests failed."
    for failed_test in "${failed_tests[@]}" ; do
        echo "  ${failed_test}"
    done
fi

if [ ${#successful_tests[@]} -gt 0 ] ; then
    echo "The following tests passed."
    for successful_test in "${successful_tests[@]}" ; do
        echo "  ${successful_test}"
    done
fi

# Return whether some tests failed.
if [ ${#failed_tests[@]} -eq 0 ] ; then
    exit 0
else
    exit 1
fi
