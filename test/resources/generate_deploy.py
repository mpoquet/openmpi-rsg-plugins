#!/usr/bin/env python3
"""Generate a deployment file matching the cluster512.xml platform file.

Usage:
  generate_deploy.py <nb-ranks> [options]
  generate_deploy.py --help

Options:
  -h --help         Show this screen.
  -i --indent SIZE  Sets indentation size [default: 2].
  -o --output FILE  If set, write the XML onto a file rather than stdout.
"""
from docopt import docopt

def generate_deployment_xml(nb_ranks, indent_size):
    indent = ' ' * indent_size
    actor_f = '''{indent}<actor host="host{n}" function="unused">
{indent}{indent}<argument value="unused"/>
{indent}</actor>'''

    actors = [actor_f.format(n=n, indent=indent) for n in range(nb_ranks)]

    return '''<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
{actors}
</platform>'''.format(actors='\n'.join(actors))

if __name__ == '__main__':
    args = docopt(__doc__)

    xml = generate_deployment_xml(int(args['<nb-ranks>']), int(args['--indent']))
    if args['--output'] is None:
        print(xml)
    else:
        with open(args['--output'], 'w') as file:
            file.write(xml + '\n')
