{
  pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {},
  kapack ? import (
    fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz"
    ) {},
    # A local version can be very convenient (e.g., to update dependencies)
    # ~/kapack
}:

# The precise versions used are not defined in this file.
# There are defined there: https://github.com/oar-team/kapack/.
# Quick guide to retrieve the versions in kapack:
# - ./default.nix defines how packages are called (e.g., select dependencies)
# - packages are defined in dedicated files (e.g., ./openmpi/default.nix)

let
  callPackage = pkgs.lib.callPackageWith(pkgs // pkgs.xlibs // self // kapack);
  self = rec {
    # OpenMPI alone
    openmpi = kapack.openmpi;

    # The plugins on their own
    rsg_plugins_local = callPackage ./local.nix {
      openmpi = kapack.openmpi;
      remote_simgrid = kapack.remote_simgrid_dev;
      simgrid = kapack.simgrid_dev_working;
    };

    # OpenMPI + the plugins
    openmpi_rsg = kapack.openmpi_rsg.override {
      openmpi = openmpi;
      openmpi_rsg_plugins = rsg_plugins_local;
    };
  };
in
  self
